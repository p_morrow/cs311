from __future__ import division
from itertools import izip
import time

__author__ = 'Pat'


class HashList(list):
    """Hash table structure for anagram classes.
    """

    #  Initialize prime array once. Primes are permuted so lowest value -> most common letter
    _primes = [5, 71, 41, 29, 2, 47, 59, 19, 11, 89, 79, 31, 43, 13, 7, 67, 97, 23, 17, 3, 37, 73, 53, 83, 61, 101,
               103, 107, 109, 113, 127, 131]

    @staticmethod
    def hash_fun(size, value):
        """Hash given string into table of length size.

        Hashes using Fundamental Theorem of Arithmetic.
        Assigns each letter a number 1-26, then takes the product of corresponding primes (from _primes). Each anagram
        class is assigned a unique hash (by FTA), and every member in a class has the same hash (by commutative property
        of multiplication on integers). This hash is then taken mod (size of array) to give an index.
        """
        hash_value = 1
        for c in value:
            hash_value = (hash_value * HashList._primes[((ord(c) - 97) % 32)]) % size
        return hash_value

    @staticmethod
    def is_anagram(s1, s2):
        """Compare two strings to determine if they are anagrams.

        Counts the appearances of letters in s1 forwards, and s2 backwards, to see if they cancel out. Counter is all
        zeros iff s1 and s2 contain entirely and exclusively the same letters.
        """
        counter = [0] * 32
        for c, d in izip(s1, s2):
            counter[(ord(c) - 97) & 0x1F] += 1
            counter[(ord(d) - 97) & 0x1F] -= 1
        return not any(counter)

    def find(self, value, hash_value):
        """Return valid index for a string, given its precomputed hash. Resizes if necessary.

        Uses an open addressing scheme with quadratic probing (constants taken from Python "dict").
        First checks if the indicated slot is open, returning that index. Otherwise, check if the head of the list there
        is in the same anagram class (and by transitivity, the whole list). If so, return it, or else permute the hash
        to probe for a new slot.
        If, when adding a new anagram class, the load factor tips over 1/2, it extends the table and finds the new index
        """
        if not self[hash_value]:
            self._load += 1
            if self._load / len(self) > 1 / 2:
                self.rebuild()
                hash_value = self.find(value, HashList.hash_fun(len(self), value))
            return hash_value
        elif self.is_anagram(value, self[hash_value][0]):
            return hash_value
        else:
            return self.find(value, (hash_value * 5 + 1) % len(self))

    def add(self, value):
        """Add a new string.
        """
        hash_value = HashList.hash_fun(len(self), value)
        index = self.find(value, hash_value)
        self[index].append(value)

    def transfer(self, value_list):
        """Add a new anagram class.
        """
        hash_value = HashList.hash_fun(len(self), value_list[0])
        index = self.find(value_list[0], hash_value)
        self[index].extend(value_list)

    def rebuild(self):
        """Extend HashList when load factor is too large.

        Quadruples size each call (this is an expensive operation).
        Hashes need to be recalculated, and each class is moved to its new location.
        """
        new = HashList(4 * len(self), self._load)
        for l in self:
            if l:
                new.transfer(l)

        self.extend([list() for _ in xrange(len(new) - len(self))])
        for l in xrange(len(self)):
            self[l] = new[l]

    def __init__(self, size=8, load=0):
        self._load = load
        list.__init__(self, [list() for _ in xrange(size)])


for dict_num in [1, 2]:
    t = time.clock()

    with open('dict' + str(dict_num)) as f:
        dictionary = (line.rstrip() for line in f)
        classes = [HashList() for _ in range(8)]  # Starts with list of 8 (length-separated) hash tables

        for word_length, i in enumerate(dictionary):
            while len(i) >= len(classes):
                classes.extend([HashList() for j in xrange(len(i))])  # If necessary, grow word length capability

            classes[len(i)].add(i)  # Primary interface with hash table

    print time.clock() - t

    with open('anagram' + str(dict_num), 'w') as f:
        for word_length in classes:
            for anagram_class in word_length:
                if anagram_class:
                    f.write(', '.join(anagram_class) + '\n')  # Exports anagram classes to file

                if dict_num == 1 and len(anagram_class) > 5:
                    print ', '.join(anagram_class)  # Prints large classes in dict1
