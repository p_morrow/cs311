import Queue
import sys

__author__ = 'Pat'

name = sys.argv[1]
cycle = []

with open(name) as G:
    """Read input into adjacency list representation."""

    size = int(G.readline()) + 1
    adjacency_list = [[] for _ in xrange(size)]

    for line in G:
        edge = [int(v) for v in line.split()]
        adjacency_list[edge[0]].append(edge[1])
        adjacency_list[edge[1]].append(edge[0])

colors = [0] * size
colors[0] = None


def find_path(source, target):
    """
    Find shortest path between source and target.

    Use to return odd-length cycle between incompatible nodes. Implements BFS.
    """

    #  Create parent attribute to each node for traceback
    parent = [0] * size
    parent[source] = target

    q = Queue.Queue(size)
    q.put(source)

    while q.not_empty:
        vertex = q.get()
        for neighbor in (_ for _ in adjacency_list[vertex] if colors[vertex]):
            if neighbor == target:
                parent[neighbor] = vertex
                x = vertex
                odd_cycle = [x]
                while x != target:
                    x = parent[x]
                    odd_cycle.append(x)
                return odd_cycle

            if not parent[neighbor]:
                parent[neighbor] = vertex
                q.put(neighbor)


# Find next uncolored subgraph
next_subgraph = (i for i in xrange(1, size) if colors[i] == 0)

for node in next_subgraph:
    #  Use DFS to color each connected subgraph.
    stack = [node]
    colors[stack[-1]] = 1
    while stack:
        vertex = stack.pop()
        for neighbor in adjacency_list[vertex]:
            if not colors[neighbor]:
                colors[neighbor] = -colors[vertex]
                stack.append(neighbor)
            elif colors[vertex] == colors[neighbor]:
                #  Remove direct path and find
                adjacency_list[vertex].remove(neighbor)
                cycle = find_path(vertex, neighbor)

                #  Exit loops
                stack = []
                next_subgraph.close()
                break

with open(name + "output", "w") as F:
    if cycle:
        F.write("No\n" + str(cycle))
    else:
        F.write("Yes\n")
        for i, j in enumerate(colors[1:], 1):
            F.write('\n' + str(i) + ':\t\t' + ['None', 'Red', 'Blue'][j])
